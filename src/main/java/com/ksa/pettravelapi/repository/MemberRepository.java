package com.ksa.pettravelapi.repository;

import com.ksa.pettravelapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
