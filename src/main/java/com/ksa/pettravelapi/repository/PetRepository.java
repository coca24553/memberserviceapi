package com.ksa.pettravelapi.repository;

import com.ksa.pettravelapi.entity.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepository extends JpaRepository<Pet, Long> {
}
