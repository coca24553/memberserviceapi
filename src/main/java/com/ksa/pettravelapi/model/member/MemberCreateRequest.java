package com.ksa.pettravelapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberCreateRequest {
    private String name;
    private String phoneNumber;
}
