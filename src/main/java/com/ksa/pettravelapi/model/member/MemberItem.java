package com.ksa.pettravelapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberItem {
    private Long id;
    private String name;
}
