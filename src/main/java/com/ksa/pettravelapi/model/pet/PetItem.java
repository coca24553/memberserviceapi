package com.ksa.pettravelapi.model.pet;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetItem {
    private Long memberId;
    private String memberName;
}
