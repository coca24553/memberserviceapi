package com.ksa.pettravelapi.model.pet;

import com.ksa.pettravelapi.enums.SizeEnum;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetCreateRequest {
    private String petName;
    @Enumerated(value = EnumType.STRING)
    private SizeEnum sizeEnum;
}
