package com.ksa.pettravelapi.model.pet;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetResponse {
    private Long memberId;
    private String memberName;
    private String memberPhoneNumber;
    private String petName;
    private String sizeEnum;
    private Double money;
}
