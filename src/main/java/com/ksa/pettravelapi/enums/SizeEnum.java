package com.ksa.pettravelapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SizeEnum {
    S("소형견", 10000.0),
    M("중형견", 15000.0),
    L("대형견", 20000.0);

    private final String size;
    private final Double money;
}
