package com.ksa.pettravelapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetTravelApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(PetTravelApiApplication.class, args);
    }

}
