package com.ksa.pettravelapi.controller;

import com.ksa.pettravelapi.model.member.MemberBaseInfoChangeRequest;
import com.ksa.pettravelapi.model.member.MemberCreateRequest;
import com.ksa.pettravelapi.model.member.MemberItem;
import com.ksa.pettravelapi.model.member.MemberResponse;
import com.ksa.pettravelapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/join")
    public String setMember(@RequestBody MemberCreateRequest request) {
        memberService.setMember(request);

        return "ok";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers() {
        return memberService.getMembers();
    }

    @GetMapping("/detail/{id}")
    public MemberResponse getMember(@PathVariable long id) {
        return memberService.getMember(id);
    }

    @PutMapping("/base-info/{id}")
    public String putBaseInfo(@PathVariable long id, @RequestBody MemberBaseInfoChangeRequest request) {
        memberService.putMemberRequest(id, request);

        return "ok";
    }
}
