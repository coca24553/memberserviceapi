package com.ksa.pettravelapi.controller;

import com.ksa.pettravelapi.entity.Member;
import com.ksa.pettravelapi.model.pet.PetCreateRequest;
import com.ksa.pettravelapi.model.pet.PetItem;
import com.ksa.pettravelapi.model.pet.PetResponse;
import com.ksa.pettravelapi.service.MemberService;
import com.ksa.pettravelapi.service.PetService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pet")
public class PetController {
    private final MemberService memberService;
    private final PetService petService;

    @PostMapping("/new/member-id/{memberId}")
    public String setPet(@PathVariable long memberId, @RequestBody PetCreateRequest request) {
        Member member = memberService.getData(memberId);
        petService.setPet(member, request);

        return "ok";
    }

    @GetMapping("/all")
    public List<PetItem> getPets() {
        return petService.getPets();
    }

    @GetMapping("/detail/{id}")
    public PetResponse getPet(@PathVariable long id) {
        return petService.getPet(id);
    }
}
