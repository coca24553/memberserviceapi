package com.ksa.pettravelapi.service;

import com.ksa.pettravelapi.entity.Member;
import com.ksa.pettravelapi.entity.Pet;
import com.ksa.pettravelapi.model.pet.PetCreateRequest;
import com.ksa.pettravelapi.model.pet.PetItem;
import com.ksa.pettravelapi.model.pet.PetResponse;
import com.ksa.pettravelapi.repository.PetRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PetService {
    private final PetRepository petRepository;

    public void setPet(Member member, PetCreateRequest request) {
        Pet addData = new Pet();
        addData.setPetName(request.getPetName());
        addData.setMember(member);
        addData.setSizeEnum(request.getSizeEnum());

        petRepository.save(addData);
    }

    public List<PetItem> getPets() {
        List<Pet> originList = petRepository.findAll();
        List<PetItem> result = new LinkedList<>();

        for (Pet pet : originList) {
            PetItem addItem = new PetItem();
            addItem.setMemberId(pet.getMember().getId());
            addItem.setMemberName(pet.getMember().getName());

            result.add(addItem);
        }
        return result;
    }

    public PetResponse getPet(long id) {
        Pet originData = petRepository.findById(id).orElseThrow();

        PetResponse response = new PetResponse();
        response.setMemberId(originData.getMember().getId());
        response.setMemberName(originData.getMember().getName());
        response.setMemberPhoneNumber(originData.getMember().getPhoneNumber());
        response.setPetName(originData.getPetName());
        response.setSizeEnum(originData.getSizeEnum().getSize());
        response.setMoney(originData.getSizeEnum().getMoney());

        return response;
    }
}
